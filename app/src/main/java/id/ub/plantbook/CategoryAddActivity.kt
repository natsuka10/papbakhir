package id.ub.plantbook

import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import id.ub.plantbook.databinding.ActivityCategoryAddBinding

class CategoryAddActivity : AppCompatActivity() {

    //View Binding
    private lateinit var binding: ActivityCategoryAddBinding

    //Firebase auth
    private lateinit var firebaseAuth: FirebaseAuth

    private lateinit var progressDialog: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCategoryAddBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //init
        firebaseAuth = FirebaseAuth.getInstance()

        //Progress dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Please Wait...")
        progressDialog.setCanceledOnTouchOutside(false)

        //handle click back
        binding.backBtn.setOnClickListener{
            onBackPressed()
        }
        // handle click submit
        binding.submitCtg.setOnClickListener {
            validateData()
        }
    }

    private var category = ""
    private fun validateData(){
        //get data
        category = binding.ctgEt.text.toString().trim()
        if (category.isEmpty()){
            Toast.makeText(this,"Category Invalid",Toast.LENGTH_SHORT).show()
        }
        else{
            addCategory()
        }
    }

    private fun addCategory(){
        progressDialog.show()
        //get timestamp for id
        val timestamp = System.currentTimeMillis()
        val hashMap = HashMap<String,Any>() // Value could be any except string for second param
        hashMap["id"] = "$timestamp"
        hashMap["category"] = category
        hashMap["timestamp"] = "$timestamp"
        hashMap["uid"] = "${firebaseAuth.uid}"

        //add to firebase db
        val reference = FirebaseDatabase.getInstance().getReference("Categories")
        reference.child("$timestamp")
            .setValue(hashMap)
            .addOnSuccessListener {
                progressDialog.dismiss()
                Toast.makeText(this,"Category Added",Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener{e->
                progressDialog.dismiss()
                Toast.makeText(this,"Failed to Add Category | ${e.message}",Toast.LENGTH_SHORT).show()
            }
    }
}