package id.ub.plantbook

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import id.ub.plantbook.databinding.ActivityDashboardAdminBinding
import id.ub.plantbook.databinding.ActivityDashboardUserBinding
import id.ub.plantbook.databinding.ActivityRegisterBinding

class DashboardAdminActivity : AppCompatActivity() {

    // View Binding
    private lateinit var binding: ActivityDashboardAdminBinding
    //Firebase authentication
    private lateinit var firebaseAuth: FirebaseAuth
    //Arraylist for categories
    private lateinit var categoryArraylist:ArrayList<ModelCategory>
    //adapter
    private lateinit var adapterCategory : AdapterCategory

    var TAG = "SEARCH_BAR_RESULT"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardAdminBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Init firebase
        firebaseAuth = FirebaseAuth.getInstance()
        checkUser()
        loadCategories()

        binding.searchBar.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                TODO("Not yet implemented")
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //called when typing
                try {
                    adapterCategory.filter!!.filter(s)
                }
                catch (e: Exception){
                    Log.d(TAG,"onTextChanged: ${e.message}")
                }
            }

            override fun afterTextChanged(s: Editable?) {
                TODO("Not yet implemented")
            }
        })

        // handle logout
        binding.logoutBtn.setOnClickListener{
            firebaseAuth.signOut()
            checkUser()
        }
        //handle add category plant
        binding.ctgBtn.setOnClickListener {
            startActivity(Intent(this,CategoryAddActivity::class.java))
        }
        //handle add Plant
        binding.addPlant.setOnClickListener{
            startActivity(Intent(this,PlantAddActivity::class.java))
        }
    }

    private fun loadCategories() {
        //iinit array list
        categoryArraylist = ArrayList()
        val reference = FirebaseDatabase.getInstance().getReference("Categories")
        reference.addValueEventListener(object:ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                //clear array before use
                categoryArraylist.clear()
                for (ds in snapshot.children){
                    //get data as model
                    val model = ds.getValue(ModelCategory::class.java)
                    //add to arraylist
                    categoryArraylist.add(model!!)
                }
                //setup adapter
                adapterCategory = AdapterCategory(this@DashboardAdminActivity,categoryArraylist)
                //set adapter to RecyclerView
                binding.ctgRecycler.adapter= adapterCategory

            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun checkUser(){
        val firebaseUser = firebaseAuth.currentUser
        if (firebaseUser == null){
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }
        else {
            val email = firebaseUser.email
            binding.subtitleTv.text = email
        }
    }
}