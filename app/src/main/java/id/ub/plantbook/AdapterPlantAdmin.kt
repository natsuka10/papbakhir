package id.ub.plantbook

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import id.ub.plantbook.databinding.RowImgAdminBinding

class AdapterPlantAdmin:RecyclerView.Adapter<AdapterPlantAdmin.holderPlantAdmin>,Filterable{

    private var context:Context
    var plantArrayList:ArrayList<ModelPlant>
    private val filterList:ArrayList<ModelPlant>
    private var filter:FilterPlantAdmin?=null
    constructor(context: Context,plantArrayList: ArrayList<ModelPlant>):super(){
        this.context=context
        this.plantArrayList=plantArrayList
        this.filterList = plantArrayList
    }
    //view Binding
    private lateinit var binding:RowImgAdminBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holderPlantAdmin {
        binding = RowImgAdminBinding.inflate(LayoutInflater.from(context),parent,false)
        return holderPlantAdmin(binding.root)
    }

    override fun getItemCount(): Int {
        return plantArrayList.size
    }

    override fun onBindViewHolder(holder: holderPlantAdmin, position: Int) {
        // Get Data,Set Data, Handle Click
        //Get Data
        val model = plantArrayList[position]
        val imgId = model.id
        val categoryId = model.categoryId
        val title = model.title
        val description = model.description
        val imgUrl = model.url
        val timestamp = model.timestamp

        //set data
        holder.titleTv.text = title
        holder.descriptionTv.text = description
        //load further details
        //load category
        MyApplication.loadCategory(categoryId,holder.categoryTvv)
        //load img thumbnail
        MyApplication.loadImgFromUrlSinglePage(imgUrl,title,holder.imgView,holder.progressBar, null)
        //load img size
        MyApplication.loadImgSize(imgUrl,title,holder.sizeTv)
        //handle click more
        holder.moreBtn.setOnClickListener{
            moreOptionsDialog(model,holder)
        }
    }

    private fun moreOptionsDialog(model: ModelPlant, holder: AdapterPlantAdmin.holderPlantAdmin) {
        val plantId = model.id
        val bookUrl = model.url
        val plantName = model.title
        //options to show dialog
        val options = arrayOf("Edit","Delete")
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Choose Option")
            .setItems(options){dialog,position->
                if (position==0){
                    val intent = Intent(context,PlantEditActivity::class.java)
                    intent.putExtra("plantId",plantId)
                    context.startActivity(intent)
                }else if (position==1){
                    MyApplication.deletePlant(context,plantId,bookUrl,plantName)
                }
            }
            .show()
    }

    /*View holder class for row_img_admin*/


    override fun getFilter(): Filter {
        if (filter==null){
            filter = FilterPlantAdmin(filterList,this)
        }
        return filter as FilterPlantAdmin
    }

    inner class holderPlantAdmin(itemView:View):RecyclerView.ViewHolder(itemView){
        //Ui Views
        val imgView = binding.imgView
        val progressBar = binding.pBarPlant
        val titleTv = binding.titleTv
        val descriptionTv = binding.descTv
        val sizeTv = binding.sizeTv
        val moreBtn = binding.moreBtn
        val categoryTvv = binding.categoryTv
    }

}