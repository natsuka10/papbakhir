package id.ub.plantbook

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.findViewTreeViewModelStoreOwner
import com.google.firebase.auth.FirebaseAuth
import id.ub.plantbook.databinding.ActivityDashboardAdminBinding
import id.ub.plantbook.databinding.ActivityDashboardUserBinding

class DashboardUserActivity : AppCompatActivity() {

    // View Binding
    private lateinit var binding: ActivityDashboardUserBinding
    //Firebase authentication
    private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //Init firebase
        firebaseAuth = FirebaseAuth.getInstance()
        checkUser()

        // handle logout
        binding.logoutBtn.setOnClickListener{
            firebaseAuth.signOut()
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }
    }
    private fun checkUser(){
        val firebaseUser = firebaseAuth.currentUser
        if (firebaseUser == null){
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }
        else {
            val email = firebaseUser.email
            binding.subtitleTv.text = email
        }
    }
}