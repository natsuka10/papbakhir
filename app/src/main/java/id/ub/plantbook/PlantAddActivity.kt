package id.ub.plantbook

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import id.ub.plantbook.databinding.ActivityPlantAddBinding

class PlantAddActivity : AppCompatActivity() {

    //view binding
    private lateinit var binding: ActivityPlantAddBinding
    //Firebase auth
    private lateinit var firebaseAuth: FirebaseAuth
    //progress dialog when adding plant
    private lateinit var progressDialog: ProgressDialog
    //Arraylist for Plant Categories
    private lateinit var categoryArrayList:ArrayList<ModelCategory>
    //uri for pick img
    private var imgUri:Uri? =  null
    //TAG
    private val TAG = "IMG_ADD_TAG"
    companion object{
        val IMAGE_REQUEST_CODE = 100
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlantAddBinding.inflate(layoutInflater)
        setContentView(binding.root)



        //init firebase auth
        firebaseAuth = FirebaseAuth.getInstance()
        loadPlantCategories()

        //setup progress dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Please Wait")
        progressDialog.setCanceledOnTouchOutside(false)

        binding.backBtn.setOnClickListener{
            onBackPressed()
        }

        binding.ctgPick.setOnClickListener{
            categoryPickDialog()
        }
        //handle pick image
        binding.imgButton.setOnClickListener{
            imgPickIntent()
        }
        //handle upload image
        binding.submitPlant.setOnClickListener {
            validateData()
        }


    }

    private var name = ""
    private var desc = ""
    private var category = ""
    private fun validateData() {
        //get Data
        name = binding.NamePText.text.toString().trim()
        desc = binding.DescPText.text.toString().trim()
        category = binding.ctgPick.text.toString().trim()

        //Validate Data
        if (name.isEmpty()){
            Toast.makeText(this,"Enter Name",Toast.LENGTH_SHORT).show()
        }
        else if (desc.isEmpty()){
            Toast.makeText(this,"Enter Description",Toast.LENGTH_SHORT).show()
        }
        else if (category.isEmpty()){
            Toast.makeText(this,"Choose Category",Toast.LENGTH_SHORT).show()
        }
        else if(imgUri == null){
            Toast.makeText(this,"Please upload an Image",Toast.LENGTH_SHORT).show()
        }
        else{
            uploadImage()
        }
    }

    private fun uploadImage() {
        Log.d(TAG,"uploadImageStorage: Upload To Storage")
        progressDialog.setMessage("Uploading Image..")
        progressDialog.show()

        //timestamp
        val timestamp = System.currentTimeMillis()

        //path of image
        val filePathandName = "Plants/$timestamp"
        //storage reference
        val storageReference = FirebaseStorage.getInstance().getReference(filePathandName)
        storageReference.putFile(imgUri!!)
            .addOnSuccessListener {taskSnapshot->
                Log.d(TAG,"uploadImage: Image Uploaded, getting url")
                val uriTask :Task<Uri> = taskSnapshot.storage.downloadUrl
                while (!uriTask.isSuccessful);
                val uploadedImgUrl = "${uriTask.result}"
                uploadImgInfotoDB(uploadedImgUrl,timestamp)
            }
            .addOnFailureListener{e->
                Log.d(TAG,"uploadImage: Failed to Upload because ${e.message}")
                progressDialog.dismiss()
                Toast.makeText(this," ${e.message}",Toast.LENGTH_SHORT).show()
            }
    }

    private fun uploadImgInfotoDB(uploadedImgUrl: String, timestamp: Long) {
        Log.d(TAG,"UploadImageToDB: Uploading to DB")
        progressDialog.setMessage("Uploading Image Info")
        //uid current user
        val uid = firebaseAuth.uid
        //setup data
        val hashMap:HashMap<String,Any> = HashMap()
        hashMap["uid"]= "$uid"
        hashMap["id"]= "$timestamp"
        hashMap["title"]= "$name"
        hashMap["description"] = "$desc"
        hashMap["categoryId"]= "$selectedCategoryId"
        hashMap["url"]= "$uploadedImgUrl"
        hashMap["timestamp"]= "$timestamp"
        hashMap["viewsCount"] = 0

        val reference = FirebaseDatabase.getInstance().getReference("Plants")
        reference.child("$timestamp")
            .setValue(hashMap)
            .addOnSuccessListener {
                Log.d(TAG,"uploadImage: Success to Upload into DB")
                progressDialog.dismiss()
                Toast.makeText(this,"Success to Upload into DB",Toast.LENGTH_SHORT).show()
                imgUri = null
            }
            .addOnFailureListener{e->
                Log.d(TAG,"uploadImage: Failed to Upload because ${e.message}")
                progressDialog.dismiss()
                Toast.makeText(this,"${e.message}",Toast.LENGTH_SHORT).show()
            }

    }

    private fun loadPlantCategories() {
        Log.d(TAG,"Load Plant Categories: Loading Plant Categories")
        categoryArrayList = ArrayList()
        //db reference
        val reference = FirebaseDatabase.getInstance().getReference("Categories")
        reference.addListenerForSingleValueEvent(object :ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                categoryArrayList.clear()
                for (ds in snapshot.children){
                    //get data
                    val model = ds.getValue(ModelCategory::class.java)
                    categoryArrayList.add(model!!)
                    Log.d(TAG,"On Data Change : ${model.category}")
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private var selectedCategoryId = ""
    private var selectedCategoryTitle = ""
    private fun categoryPickDialog(){
        Log.d(TAG,"CategoryPickDialog:Show Plant Category Dialog")
        val categoriesArray = arrayOfNulls<String>(categoryArrayList.size)
        for (i in categoryArrayList.indices){
            categoriesArray[i] =  categoryArrayList[i].category
        }
        //alert dialog
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Pick Category")
            .setItems(categoriesArray){
                dialog,which->
                //handle item click & get item
                selectedCategoryTitle = categoryArrayList[which].category
                selectedCategoryId = categoryArrayList[which].id
                //set to textview
                binding.ctgPick.text = selectedCategoryTitle
                Log.d(TAG,"CategoryPickDialog:Selected Plant Category ID : $selectedCategoryId")
                Log.d(TAG,"CategoryPickDialog:Selected Plant Category Title : $selectedCategoryTitle")
            }
            .show()
    }

    private fun imgPickIntent(){
        Log.d(TAG,"imgPickIntent: Starting img pick intent")
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent,IMAGE_REQUEST_CODE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK){
            imgUri = data?.data
            Toast.makeText(this,"Image Picked",Toast.LENGTH_SHORT).show()
        }
    }

//    val imgActivityResultLauncher = registerForActivityResult(
//        ActivityResultContracts.StartActivityForResult(),
//        ActivityResultCallback<ActivityResult> { result ->
//            if (result.resultCode == RESULT_OK){
//                Log.d(TAG,"Image Picked")
//                imgUri = result.data!!.data
//            }
//            else{
//                Log.d(TAG,"Img pick cancel")
//                Toast.makeText(this,"Cancel",Toast.LENGTH_SHORT).show()
//            }
//        }
//    )
}