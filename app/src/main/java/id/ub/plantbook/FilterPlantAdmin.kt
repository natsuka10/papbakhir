package id.ub.plantbook

import android.widget.Filter

class FilterPlantAdmin :Filter{
    //array list we want to search
    var filterlist:ArrayList<ModelPlant>
    //adapter which will implemented
    var adapterPlantAdmin:AdapterPlantAdmin

    constructor(filterlist: ArrayList<ModelPlant>, adapterPlantAdmin: AdapterPlantAdmin) : super() {
        this.filterlist = filterlist
        this.adapterPlantAdmin = adapterPlantAdmin
    }
    //Constructor


    override fun performFiltering(constraint: CharSequence?): FilterResults {
        var constraint:CharSequence? = constraint
        var results = FilterResults()
        if (constraint != null && constraint.isNotEmpty()){
            constraint = constraint.toString().uppercase()
            val filteredModels = ArrayList<ModelPlant>()
            for (i in filterlist.indices){
                if (filterlist[i].title.uppercase().contains(constraint)){
                    filteredModels.add(filterlist[i])
                }
            }
            results.count = filterlist.size
            results.values = filterlist
        }
        else{
            results.count = filterlist.size
            results.values = filterlist
        }
        return results
    }

    override fun publishResults(constraint: CharSequence?, results: FilterResults) {
        //apply filter changes
        adapterPlantAdmin.plantArrayList = results.values as ArrayList<ModelPlant>
        //notify data set changes
        adapterPlantAdmin.notifyDataSetChanged()

    }
}