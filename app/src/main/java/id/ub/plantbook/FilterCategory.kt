package id.ub.plantbook

import android.view.Display.Mode
import android.widget.Filter

class FilterCategory : Filter {

    private var filterlist : ArrayList<ModelCategory>
    private var adapterCategory:AdapterCategory

    //constructor
    constructor(filterlist: ArrayList<ModelCategory>, adapterCategory: AdapterCategory) : super() {
        this.filterlist = filterlist
        this.adapterCategory = adapterCategory
    }

    override fun performFiltering(constraint: CharSequence?): FilterResults {
        //Search valude not null & empty
        var constraint:CharSequence? = constraint
        val results = FilterResults()

        //checking null & empty
        if (constraint != null && constraint.isNotEmpty()){
            //avoid case sensitive
            constraint = constraint.toString().uppercase()
            val filteredModels:ArrayList<ModelCategory> = ArrayList()
            for (i in 0 until filterlist.size){
                //validate
                if (filterlist[i].category.uppercase().contains(constraint)){
                    //add to filtered list
                    filteredModels.add(filterlist[i])
                }
            }
            results.count = filteredModels.size
            results.values = filteredModels
        }
            else {
                results.count = filterlist.size
                results.values = filterlist
            }

        return results
    }

    override fun publishResults(constraint: CharSequence?, results: FilterResults) {
        //Apply filter
        adapterCategory.categoryArrayList = results.values as ArrayList<ModelCategory>
        //Notify Change
        adapterCategory.notifyDataSetChanged()
    }
}