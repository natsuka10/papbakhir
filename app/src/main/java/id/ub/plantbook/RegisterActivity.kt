package id.ub.plantbook

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import id.ub.plantbook.databinding.ActivityRegisterBinding

class RegisterActivity : AppCompatActivity() {
    // View Binding
    private lateinit var binding: ActivityRegisterBinding
    //Firebase authentication
    private lateinit var firebaseAuth: FirebaseAuth
    //Progress Dialog
    private lateinit var progressDialog: ProgressDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //start firebase
        firebaseAuth = FirebaseAuth.getInstance()

        //Initiate while create account
        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Please Wait")
        progressDialog.setCanceledOnTouchOutside(false)

        //Back button
        binding.backBtn.setOnClickListener{
            onBackPressed()
        }
        //Register Button
        binding.registerBtn.setOnClickListener {
            validateData()
        }
    }
    private var name = ""
    private var email = ""
    private var password = ""


    //Validate Data
    private fun validateData(){
        name = binding.nameText.text.toString().trim()
        email = binding.email2Text.text.toString().trim()
        password = binding.pass2Text.text.toString().trim()
        val cPass = binding.cPass2Text.text.toString().trim()

        //Validate
        if (name.isEmpty()) {
            Toast.makeText(this, "Invalid Name", Toast.LENGTH_SHORT).show()
        }
        else if (email.isEmpty()) {
            Toast.makeText(this, "Enter Your Password", Toast.LENGTH_SHORT).show()
        }
        else if (password.isEmpty()) {
            Toast.makeText(this, "Enter Your Password", Toast.LENGTH_SHORT).show()
        }
        else if (cPass.isEmpty()) {
            Toast.makeText(this, "Confirm Your Password", Toast.LENGTH_SHORT).show()
        }
        else if (password != cPass){
            Toast.makeText(this, "Password does Not Match", Toast.LENGTH_SHORT).show()
        }
        else {
            createAccount()
        }
    }

    private fun createAccount(){
      progressDialog.setMessage("Creating Account")
        progressDialog.show()

        //Create user
        firebaseAuth.createUserWithEmailAndPassword(email,password)
            .addOnSuccessListener {
                updateUserInfo()
            }
            .addOnFailureListener{ e->
                progressDialog.dismiss()
                Toast.makeText(this, "Failed to Create Account due to ${e.message}", Toast.LENGTH_SHORT).show()
            }
            }

    private fun updateUserInfo(){
        Toast.makeText(this, "Saving...", Toast.LENGTH_SHORT).show()
        //Save User Info
        progressDialog.setMessage("Saving Account")
        //Time created
        val timestamp = System.currentTimeMillis()
        //Get latest uid
        val uid = firebaseAuth.currentUser!!.uid

        //setup data
        val hashMap: HashMap<String,Any?> = HashMap()
        hashMap["uid"] = uid
        hashMap["email"] = email
        hashMap["name"] = name
        hashMap["profileImg"] = ""
        hashMap["userType"] = "user"
        hashMap["timestamp"] = timestamp

        //set data to db
        val ref = FirebaseDatabase.getInstance().reference.child("Users")
        if (uid != null) {
            ref.push().setValue(hashMap)
                .addOnSuccessListener {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Account Created", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@RegisterActivity,DashboardUserActivity::class.java))
                    finish()
                }
                .addOnFailureListener{e->
                    progressDialog.dismiss()
                    Toast.makeText(this, "Failed to Save Account due to ${e.message}", Toast.LENGTH_SHORT).show()
                }
        }
        else {
            Toast.makeText(this, "Failed to created", Toast.LENGTH_SHORT).show()
        }


    }
}