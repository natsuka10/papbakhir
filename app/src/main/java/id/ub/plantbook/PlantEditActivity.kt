package id.ub.plantbook

import android.app.AlertDialog
import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.Firebase
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.database
import id.ub.plantbook.databinding.ActivityPlantEditBinding

class PlantEditActivity : AppCompatActivity() {

    //view binding
    private lateinit var binding:ActivityPlantEditBinding

    private companion object{
        private const val TAG = "PLANT_EDIT_TAG"
    }
    //book id from intent AdapterPlant
    private var plantId = ""
    //progress dialog
    private lateinit var progressDialog: ProgressDialog
    //arraylist to hold category
    private lateinit var categoryTitleArrayList: ArrayList<String>
    //arraylist to hold categoryid
    private lateinit var categoryIdArrayList: ArrayList<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlantEditBinding.inflate(layoutInflater)
        setContentView(binding.root)
        plantId = intent.getStringExtra("plantId")!!
        //setup progress dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Please Wait")
        progressDialog.setCanceledOnTouchOutside(false)

        loadCategories()
        loadPlantInfo()

        //handle backbtn
        binding.backBtn.setOnClickListener{
            onBackPressed()
        }
        //handle pick category
        binding.ctgPick.setOnClickListener{
            categoryDialog()
        }
        binding.submitPlant.setOnClickListener{
            validateData()
        }
    }

    private fun loadPlantInfo() {
        Log.d(TAG,"loadPlantInfo: Loading Plant Info")
        val reference = FirebaseDatabase.getInstance().getReference("Plants")
        reference.child(plantId)
            .addListenerForSingleValueEvent(object :ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    //get plant info
                    selectedCategoryId = snapshot.child("categoryId").value.toString()
                    val description = snapshot.child("description").value.toString()
                    val title = snapshot.child("title").value.toString()
                    //set to view
                    binding.NamePText.setText(title)
                    binding.DescPText.setText(description)

                    //load book
                    Log.d(TAG,"onDataChange : load Category")
                    val refPlantCategory = FirebaseDatabase.getInstance().getReference("Categories")
                    refPlantCategory.child(selectedCategoryId)
                        .addListenerForSingleValueEvent(object :ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val category = snapshot.child("category").value
                                binding.ctgPick.text = category.toString()
                            }

                            override fun onCancelled(error: DatabaseError) {

                            }
                        })
                }

                override fun onCancelled(error: DatabaseError) {

                }
            })
    }

    private var title = ""
    private var description = ""

    private fun validateData() {
        //get data
        title = binding.NamePText.text.toString().trim()
        description = binding.DescPText.text.toString().trim()
        if (title.isEmpty()){
            Toast.makeText(this,"Enter Plant Name",Toast.LENGTH_SHORT).show()
        }
        else if (description.isEmpty()){
            Toast.makeText(this,"Enter Plant Name",Toast.LENGTH_SHORT).show()
        }
        else{
            updatePlant()
        }
    }

    private fun updatePlant() {
        Log.d(TAG,"Start updating plant")
        progressDialog.setMessage("Updating Plant Info")
        progressDialog.show()

        //setup data
        val hashMap = HashMap<String,Any>()
        hashMap["title"] = "$title"
        hashMap["description"] = "$description"
        hashMap["categoryId"] = "$selectedCategoryId"

        val reference = FirebaseDatabase.getInstance().getReference("Plants")
        reference.child(plantId)
            .updateChildren(hashMap)
            .addOnSuccessListener {
                progressDialog.dismiss()
                Log.d(TAG,"updatePlant: Update Succeeded")
                Toast.makeText(this,"Update Succeeded",Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener {e->
                Log.d(TAG,"updatePlant: Failed to update due to ${e.message}")
                Toast.makeText(this,"Failed to update due to ${e.message}",Toast.LENGTH_SHORT).show()
            }
    }


    private var selectedCategoryId = ""
    private var selectedCategoryTitle = ""
    private fun categoryDialog() {
        val categoriesArray = arrayOfNulls<String>(categoryTitleArrayList.size)
        for (i in categoryTitleArrayList.indices){
            categoriesArray[i] = categoryTitleArrayList[i]
        }
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Choose Category")
            .setItems(categoriesArray){dialog,position->
                selectedCategoryId = categoryIdArrayList[position]
                selectedCategoryTitle = categoryTitleArrayList[position]
                binding.ctgPick.text = selectedCategoryTitle
            }
            .show()
    }

    private fun loadCategories() {
        Log.d(TAG,"loadCategories: Loading Categories")
        categoryTitleArrayList = ArrayList()
        categoryIdArrayList = ArrayList()
        val reference = FirebaseDatabase.getInstance().getReference("Categories")
        reference.addListenerForSingleValueEvent(object :ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                categoryIdArrayList.clear()
                categoryTitleArrayList.clear()
                for (ds in snapshot.children){
                    val id = "${ds.child("id").value}"
                    val category = "${ds.child("category").value}"
                    categoryIdArrayList.add(id)
                    categoryTitleArrayList.add(category)
                    Log.d(TAG,"On data change: Category ID ${id}")
                    Log.d(TAG,"On data change: Category Title ${category}")
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }
}