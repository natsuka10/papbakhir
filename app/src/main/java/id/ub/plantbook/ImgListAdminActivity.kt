package id.ub.plantbook

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Display.Mode
import androidx.core.widget.addTextChangedListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import id.ub.plantbook.databinding.ActivityImgListAdminBinding

class ImgListAdminActivity : AppCompatActivity() {

    private lateinit var binding:ActivityImgListAdminBinding

    private companion object{
        const val TAG = "IMG_LIST_ADMIN"
    }

    private var categoryId = ""
    private var category = ""

    //Arraylist for Plant
    private lateinit var plantArrayList: ArrayList<ModelPlant>
    //adapter
    private lateinit var adapterPlantAdmin: AdapterPlantAdmin
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityImgListAdminBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val intent = intent
        categoryId = intent.getStringExtra("categoryId")!!
        category = intent.getStringExtra("category")!!
        //set pdf category
        binding.subtitleList.text = category
        //load plant
        loadPlantList()
        //handle back button
        binding.backBtn.setOnClickListener{
            onBackPressed()
        }
        //search
        binding.searchBar.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    adapterPlantAdmin.filter!!.filter(s)
                }
                catch (e:Exception){
                    Log.d(TAG,"onTextChanged: ${e.message}")
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })

    }
    private fun loadPlantList(){
        //init arraylist
        plantArrayList= ArrayList()
        val reference = FirebaseDatabase.getInstance().getReference("Plants")
        reference.orderByChild("categoryId").equalTo(categoryId)
            .addValueEventListener(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    plantArrayList.clear()
                    for (ds in snapshot.children){
                        //get data
                        val model = ds.getValue(ModelPlant::class.java)
                        if (model !=null){
                            plantArrayList.add(model)
                            Log.d(TAG,"On data change: ${model.title} ${model.categoryId}")
                        }
                    }
                    adapterPlantAdmin = AdapterPlantAdmin(this@ImgListAdminActivity,plantArrayList)
                    binding.plantRv.adapter = adapterPlantAdmin
                }

                override fun onCancelled(error: DatabaseError) {

                }
            })

    }
}