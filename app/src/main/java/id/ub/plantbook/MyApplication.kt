package id.ub.plantbook

import android.app.Application
import android.app.ProgressDialog
import android.content.Context
import android.icu.text.CaseMap.Title
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import java.util.Calendar
import java.util.Locale
import com.squareup.picasso.Picasso

class MyApplication:Application() {
    override fun onCreate() {
        super.onCreate()
    }
    companion object{
        //static method for convert timestamp
        fun formatTimeStamp(timestamp: Long):String{
            val cal = Calendar.getInstance(Locale.ENGLISH)
            cal.timeInMillis=timestamp
            //format dd/MM/yyyy
            return DateFormat.format("dd/MM/yyyy",cal).toString()
        }
        fun loadImgSize(imgUrl:String,imgTitle:String,sizeTv: TextView){
            val TAG = "IMG_SIZE_TAG"
            val reference = FirebaseStorage.getInstance().getReferenceFromUrl(imgUrl)
            reference.metadata
                .addOnSuccessListener {storageMetaData->
                    Log.d(TAG,"loadImgSize:got metadata")
                    val bytes = storageMetaData.sizeBytes.toDouble()
                    Log.d(TAG,"loadImgSize:Size Bytes ${bytes}")
                    //convert bytes
                    val kb = bytes/1024
                    val mb = kb/1024
                    if (mb>=1){
                        sizeTv.text="${String.format("%.2f",mb)} MB"
                    }
                    else if (kb>=1){
                        sizeTv.text="${String.format("%.2f",kb)} KB"
                    }
                    else{
                        sizeTv.text="${String.format("%.2f",bytes)} Bytes"
                    }

                }
                .addOnFailureListener{e->
                    Log.d(TAG,"loadImgSize : Failed to get Metadata because ${e.message}")
                }
        }
        fun loadImgFromUrlSinglePage(
            imgUrl:String,
            imgTitle: String,
            imgView: ImageView,
            progressBar: ProgressBar,
            pagesTv: TextView?
        ){
            val TAG = "IMG_THUMBNAIL_TAG"
            val reference = FirebaseStorage.getInstance().getReferenceFromUrl(imgUrl)
            reference.getBytes(Constants.MAX_BYTES_IMG)
                .addOnSuccessListener {bytes->
                    Log.d(TAG,"loadImgSize:got metadata")
                    Log.d(TAG,"loadImgSize:Size Bytes ${bytes}")
                    //Set to ImgView
                    if (Picasso.get().load(imgUrl).into(imgView) !=null){
                        progressBar.visibility = View.INVISIBLE
                    }
                    else{
                        progressBar.visibility = View.VISIBLE
                    }

                }
                .addOnFailureListener{e->
                    Log.d(TAG,"loadImgSize : Failed to get Metadata because ${e.message}")
                }
        }

        fun loadCategory(categoryId:String,categoryTv:TextView){
            val reference = FirebaseDatabase.getInstance().getReference("Categories")
            reference.child(categoryId)
                .addListenerForSingleValueEvent(object :ValueEventListener{
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val category = "${snapshot.child("category").value}"
                        categoryTv.text = category
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }

        fun deletePlant(context: Context,plantId:String,imgUrl: String,plantTitle: String){
            val TAG = "DELETE_PLANT_TAG"
            Log.d(TAG,"deletePlant: Deleting Plant")
            val progressDialog = ProgressDialog(context)
            progressDialog.setTitle("Please wait")
            progressDialog.setMessage("Deleting $plantTitle")
            progressDialog.setCanceledOnTouchOutside(false)
            progressDialog.show()
            Log.d(TAG,"deletePlant: Deleting Plant on Storage")
            val storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(imgUrl)
            storageReference.delete()
                .addOnSuccessListener {
                    Log.d(TAG,"deletePlant: Deleted Storage Success")
                    Log.d(TAG,"deletePlant: Deleting Plant on DB")
                    val reference = FirebaseDatabase.getInstance().getReference("Plants")
                    reference.child(plantId)
                        .removeValue()
                        .addOnSuccessListener {
                            progressDialog.dismiss()
                            Toast.makeText(context,"Deleted from DB",Toast.LENGTH_SHORT).show()
                            Log.d(TAG,"deletePlant: Deleted from DB Success")

                        }
                        .addOnFailureListener{e->
                            progressDialog.dismiss()
                            Log.d(TAG,"deletePlant: Failed Delete from db | ${e.message}")
                            Toast.makeText(context,"Failed Delete from db | ${e.message}",Toast.LENGTH_SHORT).show()
                        }
                }
                .addOnFailureListener{e->
                    progressDialog.dismiss()
                    Log.d(TAG,"deletePlant: Failed Delete | ${e.message}")
                    Toast.makeText(context,"Failed Delete | ${e.message}",Toast.LENGTH_SHORT).show()

                }
        }

    }

}