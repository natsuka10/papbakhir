package id.ub.plantbook

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.google.firebase.database.FirebaseDatabase
import id.ub.plantbook.databinding.RowCategoryBinding


class AdapterCategory : RecyclerView.Adapter<AdapterCategory.HolderCategory>,Filterable{

    private val context : Context
    public var categoryArrayList:ArrayList<ModelCategory>
    private lateinit var binding: RowCategoryBinding
    private var filterList:ArrayList<ModelCategory>
    private var filter:FilterCategory? = null

    //Constructor
    constructor(context: Context,categoryArrayList: ArrayList<ModelCategory>){
        this.context = context
        this.categoryArrayList = categoryArrayList
        this.filterList = categoryArrayList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderCategory {
        binding = RowCategoryBinding.inflate(LayoutInflater.from(context),parent,false)
        return HolderCategory(binding.root)
    }

    override fun getItemCount(): Int {
        return categoryArrayList.size
    }

    override fun onBindViewHolder(holder: HolderCategory, position: Int) {
        //get data,set data, handle click

        //get data
        val model = categoryArrayList[position]
        val id = model.id
        val category = model.category
        val uid = model.uid
        val timestamp = model.timestamp

        //set data
        holder.ctgTitle.text = category

        //handle click delete
        holder.deleteBtn.setOnClickListener{
            val builder = AlertDialog.Builder(context)
            builder.setTitle("Delete")
                .setMessage("Are you sure want to delete this category?")
                .setPositiveButton("Confirm"){a,d->
                    Toast.makeText(context,"Deleting...",Toast.LENGTH_SHORT).show()
                    deleteCategory(model,holder)
                }
                .setNegativeButton("Cancel"){a,d->
                    a.dismiss()
                }
                .show()
        }
        holder.itemView.setOnClickListener{
            val intent = Intent(context,ImgListAdminActivity::class.java)
            intent.putExtra("categoryId",id)
            intent.putExtra("category",category)
            context.startActivity(intent)
        }
    }



    private fun deleteCategory(model: ModelCategory, holder: HolderCategory) {
        //get id
        val id = model.id
        //Firebase DB > Categories > categoryId
        val reference = FirebaseDatabase.getInstance().getReference("Categories")
        reference.child(id)
            .removeValue()
            .addOnSuccessListener {
                Toast.makeText(context,"Category Deleted",Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener{e->
                Toast.makeText(context,"Failed to Delete | ${e.message}",Toast.LENGTH_SHORT).show()
            }

    }

    //ViewHolder class to hold/init UI View for row_category
    inner class HolderCategory(itemView: View): ViewHolder(itemView){
        var ctgTitle:TextView = binding.ctgTitle
        var deleteBtn:ImageButton = binding.deleteBtn
    }

    override fun getFilter(): Filter {
        if (filter==null){
            filter = FilterCategory(filterList,this)
        }
        return filter as FilterCategory
    }


}