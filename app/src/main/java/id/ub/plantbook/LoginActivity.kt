package id.ub.plantbook

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.datastore.preferences.protobuf.Value
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import id.ub.plantbook.databinding.ActivityLoginBinding
import id.ub.plantbook.databinding.ActivityRegisterBinding

class LoginActivity : AppCompatActivity() {

    // View Binding
    private lateinit var binding: ActivityLoginBinding
    //Firebase authentication
    private lateinit var firebaseAuth: FirebaseAuth
    //Progress Dialog
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //start firebase
        firebaseAuth = FirebaseAuth.getInstance()

        //Initiate while log into account
        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Please Wait")
        progressDialog.setCanceledOnTouchOutside(false)

        //handle click dont have account
        binding.newUser.setOnClickListener{
            startActivity(Intent(this,RegisterActivity::class.java))
        }
        //handle click login
        binding.loginBtn.setOnClickListener{
            validateData()
        }

    }

    private var email = ""
    private var password = ""

    //Validate Data
    private fun validateData(){
        email = binding.emailText.text.toString().trim()
        password = binding.passText.text.toString().trim()

        //Validate
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(this, "Invalid Email", Toast.LENGTH_SHORT).show()
        }
        else if (password.isEmpty()) {
            Toast.makeText(this, "Invalid Password", Toast.LENGTH_SHORT).show()
        }
        else{
            loginUser()
        }
    }
    private fun loginUser(){
        progressDialog.setMessage("Logging In")
        progressDialog.show()

        firebaseAuth.signInWithEmailAndPassword(email,password)
            .addOnSuccessListener {
                checkUser()
            }
            .addOnFailureListener{e->
                progressDialog.dismiss()
                Toast.makeText(this, "Failed to Log In due to ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }
    private fun checkUser(){
        progressDialog.setMessage("Checking User")
        val firebaseUser = firebaseAuth.currentUser!!
        val ref = FirebaseDatabase.getInstance().getReference("Users")
        if (firebaseUser.uid == "3IPt6fyUvHW1JhzDojriNS0HNc22") {
            startActivity(Intent(this@LoginActivity,DashboardAdminActivity::class.java))
        }
        else{
            startActivity(Intent(this@LoginActivity,DashboardUserActivity::class.java))
        }
    }
}